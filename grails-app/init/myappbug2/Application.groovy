package myappbug2

import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration
import groovy.transform.CompileStatic
import org.springframework.context.EnvironmentAware
import org.springframework.core.env.Environment
import org.springframework.core.env.MapPropertySource

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class Application extends GrailsAutoConfiguration implements EnvironmentAware {

    static void main(String[] args) {
        GrailsApp.run(Application, args)
    }

    @Override
    void setEnvironment(Environment environment) {
        Map configx = ['server.port': '8083']
        environment.propertySources.addFirst(new MapPropertySource("appConfig", configx))
        println "port: ${environment.getProperty('server.port')}"
//-----------------------------------------------
        String configFileName = System.getenv('nsl_services_config') ?: "c:/Users/chris/.nsl/services-config-g3.groovy"
        File configBase = new File(configFileName)
        if (configBase.exists()) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
            println "Loading external configuration from Groovy: ${configBase.absolutePath} ${LocalDateTime.now().format(dtf)}"
            def config = new ConfigSlurper().parse(configBase.toURI().toURL())
            println "load database url: ${config.dataSource.url} ${LocalDateTime.now().format(dtf)}"
            environment.propertySources.addFirst(new MapPropertySource("appConfig2", config))
        } else {
            println "External config could not be found, checked ${configBase.absolutePath}"
        }
        println "porty: ${environment.getProperty('server.port')}"
    }

}
